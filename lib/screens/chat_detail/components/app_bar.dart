import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

import '../../../constants/routes_constant.dart';
import '../../../constants/theme_constant.dart';
import '../../../models/user_model.dart';
import '../../../providers/auth_provider.dart';

class ChatDetailAppBar extends StatelessWidget with PreferredSizeWidget {
  const ChatDetailAppBar({Key? key, required this.handleChangeTheme})
      : super(key: key);

  final Function(String) handleChangeTheme;

  void _goToOptions(BuildContext context) {
    UserModel? user = context.read<Auth>().user;
    Get.toNamed(Routes.chatOptions, arguments: [user, handleChangeTheme]);
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Theme.of(context).primaryColor,
      elevation: 1,
      automaticallyImplyLeading: true,
      title: GestureDetector(
        onTap: () => _goToOptions(context),
        child: Row(
          children: [
            Stack(children: [
              const CircleAvatar(
                radius: 16,
                backgroundImage: AssetImage('assets/images/avatar.jpg'),
              ),
              Positioned(
                right: 0,
                bottom: 0,
                child: Container(
                  height: 10,
                  width: 10,
                  decoration: BoxDecoration(
                    color: Colors.green,
                    shape: BoxShape.circle,
                    border: Border.all(
                        color: Theme.of(context).scaffoldBackgroundColor,
                        width: 1),
                  ),
                ),
              )
            ]),
            const SizedBox(width: 12),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  'Nguyen Tan',
                  style: TextStyle(fontSize: 16),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 2.0),
                  child: Text(
                    'chat_detail_offline'.trParams({
                      'time': '10m',
                    }),
                    style: const TextStyle(
                        fontSize: 11, fontWeight: FontWeight.normal),
                  ),
                )
              ],
            )
          ],
        ),
      ),
      actions: [
        const Icon(Icons.call, size: 24),
        Stack(alignment: Alignment.centerRight, children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: kDefaultPadding * 2 / 3),
            child: Icon(Icons.videocam, size: 24),
          ),
          Positioned(
            right: 2,
            child: Container(
              height: 8,
              width: 8,
              decoration: const BoxDecoration(
                color: Colors.green,
                shape: BoxShape.circle,
              ),
            ),
          )
        ]),
        IconButton(
          onPressed: () => _goToOptions(context),
          icon: const Icon(Icons.info_rounded, size: 24),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
