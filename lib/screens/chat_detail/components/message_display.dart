import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lab6/controllers/messages_controller.dart';
import 'package:lab6/screens/chat_detail/models/message_model.dart';

import 'message.dart';

class MessageDisplay extends StatefulWidget {
  const MessageDisplay({Key? key}) : super(key: key);

  @override
  State<MessageDisplay> createState() => _MessageDisplayState();
}

class _MessageDisplayState extends State<MessageDisplay> {
  final MessagesController _messagesController = Get.put(MessagesController());

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Obx(
        () => ListView.builder(
          reverse: true,
          itemCount: _messagesController.messages.length,
          itemBuilder: (context, index) {
            final MessageModel message = _messagesController.messages[index];
            final bool isMe = message.sender == "nguyentan2808";
            bool isShowAvatar;

            if (index == 0) {
              isShowAvatar = true;
            } else {
              final MessageModel nextMessage =
                  _messagesController.messages[index - 1];
              isShowAvatar = message.sender != nextMessage.sender;
            }

            return Message(
                message: message, isMe: isMe, isShowAvatar: isShowAvatar);
          },
        ),
      ),
    );
  }
}
